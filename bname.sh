# Bulk rename
#!/bin/bash

# bname (bulk name) is a small utility script which renames files in current
# folder according to a filename pattern given by the user.
# The script (should) save the files' file extentions
#
# The script only renames the part given by the user
# eg. a file named "elmos-file" will be renamed to "mattis-file" if
# the user gives a rename-pattern of "elmos"
#
# A concoction of Elmo Rohula

file_pattern1=$1
pattern_length=${#file_pattern1}
rename_pattern1=$2

if [[ -z "$file_pattern1" || -z "$rename_pattern!" ]]; then
	echo "Please provide a pattern for existing files"
	echo "and a pattern with which to rename them"
	echo "eg. \"bname pattern new-file-pattern\""
	echo "bname renames only files on the current folder"
	exit
fi

for i in $file_pattern1*; do
	if [[ -d $i ]]; then
		echo "$i is a directory, not renaming"
		echo "----"
		continue
	fi	
	
	# file_ext will be empty if file has no file-extention
	file_remainder=$(echo $i | sed "s/$file_pattern1//" | cut -d '.' -f 1)
	file_ext=$(echo $i | cut -s -d '.' -f 2)
	
	echo "Old file name: "$i
	if [[ -z $file_ext ]]; then
		new_file_name=$(echo "$rename_pattern1""$file_remainder")	
		echo "New file name: ""$new_file_name"
		mv $i $new_file_name
	else
		new_file_name=$(echo "$rename_pattern1""$file_remainder"."$file_ext")
		echo "New file name: ""$new_file_name"
		mv $i $new_file_name
	fi
	echo "----"
done
